package com.example.x0

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var button1: Button
    private lateinit var button2: Button
    private lateinit var button3: Button
    private lateinit var button4: Button
    private lateinit var button5: Button
    private lateinit var button6: Button
    private lateinit var button7: Button
    private lateinit var button8: Button
    private lateinit var button9: Button
    private lateinit var reset: Button
    private var activePlayer = 1
    private var motamashe1 = ArrayList<Int>()
    private var motamashe2 = ArrayList<Int>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        reset = findViewById(R.id.reset)
        reset.setOnClickListener{
            dareseteba()
        }
    }
    private fun init() {
        button1 = findViewById(R.id.button1)
        button2 = findViewById(R.id.button2)
        button3 = findViewById(R.id.button3)
        button4 = findViewById(R.id.button4)
        button5 = findViewById(R.id.button5)
        button6 = findViewById(R.id.button6)
        button7 = findViewById(R.id.button7)
        button8 = findViewById(R.id.button8)
        button9 = findViewById(R.id.button9)

        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)


    }
    override fun onClick(clickedView: View?) {
        if (clickedView is Button) {
            var buttonNumber = 0
            when (clickedView.id) {
                R.id.button1 -> buttonNumber = 1
                R.id.button2 -> buttonNumber = 2
                R.id.button3 -> buttonNumber = 3
                R.id.button4 -> buttonNumber = 4
                R.id.button5 -> buttonNumber = 5
                R.id.button6 -> buttonNumber = 6
                R.id.button7 -> buttonNumber = 7
                R.id.button8 -> buttonNumber = 8
                R.id.button9 -> buttonNumber = 9

            }
            if (buttonNumber != 0) {
                playGame(clickedView, buttonNumber)
            }

        }
    }
    private fun playGame(clickedView: Button, buttonNumber: Int) {
        if(activePlayer == 1 ) {
            clickedView.text = "x"
            clickedView.setBackgroundColor(Color.RED)
            motamashe1.add(buttonNumber)

            activePlayer = 2
        }
        else {
                clickedView.text = "0"
            clickedView.setBackgroundColor(Color.RED)
            motamashe2.add(buttonNumber)
            activePlayer = 1
            }
        clickedView.isEnabled = false

        check()

        }
    private fun check() {
        var winnerMotamashe = 0
        if (motamashe1.contains(1) && motamashe1.contains(2) && motamashe1.contains(3)) {
            winnerMotamashe = 1

        }
        if (motamashe2.contains(1) && motamashe2.contains(2) && motamashe2.contains(3)) {
            winnerMotamashe = 1
        }
        if (motamashe1.contains(4) && motamashe1.contains(5) && motamashe1.contains(6)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(4) && motamashe2.contains(5) && motamashe2.contains(6)) {
            winnerMotamashe = 2

        }
        if (motamashe1.contains(7) && motamashe1.contains(8) && motamashe1.contains(9)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(7) && motamashe2.contains(8) && motamashe2.contains(9)) {
            winnerMotamashe = 2

        }
        if (motamashe1.contains(1) && motamashe1.contains(4) && motamashe1.contains(7)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(1) && motamashe2.contains(4) && motamashe2.contains(7)) {
            winnerMotamashe = 2

        }
        if (motamashe1.contains(2) && motamashe1.contains(5) && motamashe1.contains(8)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(2) && motamashe2.contains(5) && motamashe2.contains(8)) {
            winnerMotamashe = 2

        }
        if (motamashe1.contains(3) && motamashe1.contains(6) && motamashe1.contains(9)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(3) && motamashe2.contains(6) && motamashe2.contains(9)) {
            winnerMotamashe = 2

        }
        if (motamashe1.contains(1) && motamashe1.contains(5) && motamashe1.contains(9)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(1) && motamashe2.contains(5) && motamashe2.contains(9)) {
            winnerMotamashe = 2

        }
        if (motamashe1.contains(3) && motamashe1.contains(5) && motamashe1.contains(7)) {
            winnerMotamashe = 2

        }
        if (motamashe2.contains(3) && motamashe2.contains(5) && motamashe2.contains(7)) {
            winnerMotamashe = 2

        }
        if (winnerMotamashe == 1) {
            Toast.makeText(this, "PIRVELOmogige glocav!", Toast.LENGTH_SHORT).show()
        } else if (winnerMotamashe == 2 ) {
            Toast.makeText(this,"MEOREVmoige glocav!",Toast.LENGTH_SHORT).show()

        }

    }

    private fun dareseteba() {
        button1.setBackgroundColor(Color.rgb(79,31,165))
        button2.setBackgroundColor(Color.rgb(79,31,165))
        button3.setBackgroundColor(Color.rgb(79,31,165))
        button4.setBackgroundColor(Color.rgb(79,31,165))
        button5.setBackgroundColor(Color.rgb(79,31,165))
        button6.setBackgroundColor(Color.rgb(79,31,165))
        button7.setBackgroundColor(Color.rgb(79,31,165))
        button8.setBackgroundColor(Color.rgb(79,31,165))
        button9.setBackgroundColor(Color.rgb(79,31,165))

        button1.text = ""
        button2.text = ""
        button3.text = ""
        button4.text = ""
        button5.text = ""
        button6.text = ""
        button7.text = ""
        button8.text = ""
        button9.text = ""
        button1.isEnabled = true
        button2.isEnabled = true
        button3.isEnabled = true
        button4.isEnabled = true
        button5.isEnabled = true
        button6.isEnabled = true
        button7.isEnabled = true
        button8.isEnabled = true
        button9.isEnabled = true
        activePlayer = 1
        motamashe1.clear()
        motamashe2.clear()

    }


}